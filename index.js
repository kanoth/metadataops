var express = require('express'),
    exphbs  = require('express3-handlebars'),
    app = express();
hbs = exphbs.create({
    // Specify helpers which are only registered on this instance.
    helpersa: {
        foo: function () { return 'FOO!'; },
        bar: function () { return 'BAR!'; }
    },
    defaultLayout: 'main'
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.use(express.static(process.cwd() + '/public'));


app.get('/', function (req, res) {
    res.render('home');
});

port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log("Server listening on port " + port + " in " + app.settings.env + " mode.");
});